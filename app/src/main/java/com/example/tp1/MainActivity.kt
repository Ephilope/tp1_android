package com.example.tp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.tp1.databinding.ActivityMainBinding

var TAG:String= "TP1"

class MainActivity : AppCompatActivity()  , View.OnClickListener{
    private lateinit var ui: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        // initialisation interne de l'activité
        super.onCreate(savedInstanceState)
        val intent : Intent = intent
        var information : String? =intent.getStringExtra("username")

        // mise en place du layout par un view binding
        ui = ActivityMainBinding.inflate(layoutInflater)
        setContentView(ui.root)
        // titre de l'activité
        setTitle(getLocalClassName());
        title = localClassName
        ui.bouton2.setOnClickListener {
            compteur += 2
            var app : TP1Application = applicationContext as TP1Application
            var compteur : Int = app.publicCompteur
            ui.texte.text = "compteur = $compteur"
        }
        ui.texte.setText("Bonjour "+information)
        ui.bouton3.setOnClickListener(this::onBouton3Click)
        ui.bouton4.setOnClickListener(this)
        ui.bouton5.setOnClickListener(this::onBouton5Click);
        ui.bouton6.setOnClickListener(this::onBouton6Click);

    }
    private fun onBouton3Click(view: View?) {
        compteur *= 2
        ui.texte.text = "compteur = $compteur"
    }
    private fun onBouton5Click(view: View?) {
        val intent : Intent = Intent(this, InfosActivity::class.java);
        startActivity(intent)
    }
    private fun onBouton6Click(view: View?) {
        val intent : Intent = Intent(this, InfosActivity::class.java);
        startActivity(intent)
        finish()
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "dans $localClassName .onDestroy")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "dans $localClassName .onPause")
    }
    override fun onResume() {
        super.onResume()
        Log.i(TAG, "dans $localClassName .onResume")
    }
    private var compteur = 0


    fun onBouton1Click(view: View) {
        compteur += 1
        ui.texte.text = "compteur = $compteur"
    }
    override fun onClick(p0: View?) {
        compteur *= 5;
        ui.texte.text = "compteur= $compteur "

    }

    }