package com.example.tp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.tp1.databinding.ActivityInfosBinding

class InfosActivity : AppCompatActivity() {
    private lateinit var ui: ActivityInfosBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(getLocalClassName());
        setContentView(R.layout.activity_infos);
        ui = ActivityInfosBinding.inflate(layoutInflater)
        setContentView(ui.root)
        ui.bouton1.setOnClickListener(this::onBoutonRetourClick);
    }
    private fun onBoutonRetourClick(view: View?) {
        finish()
    }
}