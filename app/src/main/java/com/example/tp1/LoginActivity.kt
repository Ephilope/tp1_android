package com.example.tp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.example.tp1.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    private lateinit var ui: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(getLocalClassName());
        setContentView(R.layout.activity_login);
        ui = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(ui.root)
        ui.username.setOnEditorActionListener(this::onEditorAction);
    }
    private fun onEditorAction(textView: TextView?, actionId: Int, keyEvent: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            // Texte saisi
            val username = ui.username.text.toString()
            val information: String = username

            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("username", information)
            startActivity(intent)

            finish()

            return true
        } else {
            // Événement non consommé
            return false
        }
    }


}