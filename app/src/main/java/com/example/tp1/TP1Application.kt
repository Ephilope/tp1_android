package com.example.tp1
import android.app.Application

public class TP1Application : Application() {
    private var compteur: Int = 0

    var publicCompteur: Int
        get() = compteur
        set(value) {
            compteur = value
        }
    override public  fun onCreate() {
        super.onCreate()
        compteur = 0
    }
}